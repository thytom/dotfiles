call plug#begin('~/.vim/plugged')
" Colorscheme
	Plug 'dracula/vim', { 'as': 'dracula' }

" UI
	Plug 'itchyny/lightline.vim'
	Plug 'preservim/nerdtree'

" Text Editing
	Plug 'jiangmiao/auto-pairs'
	Plug 'junegunn/goyo.vim'
	Plug 'terryma/vim-multiple-cursors'
	Plug 'tomtom/tcomment_vim'
	Plug 'tpope/vim-surround'

" Dependencies
	Plug 'inkarkat/vim-SyntaxRange'
	Plug 'jalvesaq/R-Vim-runtime'
	Plug 'plasticboy/vim-markdown'
	Plug 'vim-pandoc/vim-pandoc'
	Plug 'vim-pandoc/vim-pandoc-syntax'
	Plug 'ryanoasis/vim-devicons'

" TeX/LaTeX/Rmd
	" Plug 'matze/vim-tex-fold'
	Plug 'vim-pandoc/vim-rmarkdown'

" Haskell
	Plug 'alx741/vim-hindent'
	Plug 'neovimhaskell/haskell-vim'
	Plug 'ndmitchell/ghcid', { 'rtp': 'plugins/nvim' }

" Web/Database
	" Plug 'has2k1/sql.vim'
	Plug 'rstacruz/sparkup'
	Plug 'captbaritone/better-indent-support-for-php-with-html'
	Plug 'StanAngeloff/php.vim'

" C++
	Plug 'bfrg/vim-cpp-modern'

" Rust
	" Plug 'rust-lang/rust.vim'
call plug#end()
