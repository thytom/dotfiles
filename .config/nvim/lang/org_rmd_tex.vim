" LaTeX Mappings
autocmd FileType plaintex nnoremap <buffer> <leader>p :!lualatex %<CR><CR>
autocmd FileType plaintex nnoremap <buffer> <leader>o :!zathura %<.pdf &<CR><CR>
autocmd FileType plaintex nnoremap <buffer> <leader>z i\section{}<ESC>i
autocmd FileType plaintex nnoremap <buffer> <leader><leader>z i\subsection{}<ESC>i
autocmd FileType plaintex nnoremap <buffer> <leader><leader><leader>z i\subsubsection{}<ESC>i
autocmd FileType plaintex nnoremap <buffer> <leader>bo a\textbf{
autocmd FileType plaintex nnoremap <buffer> <leader>be a\begin{<++>}<CR>\end{<++>}<ESC>Vk:s/<++>/

"R Markdown Mappings
inoremap <C-d> <C-R>=strftime("%F %H:%M")<CR>
autocmd FileType rmarkdown, markdown, plaintex nnoremap <buffer> <leader>e :call RenderR()<CR>
"Code block
nnoremap <leader>co i```<CR><CR>```<ESC>kkA

function RenderR()
	execute "!echo \"require(rmarkdown); render('". bufname("%") . "')\" \| R --vanilla"
	let texfilename = expand('%:t:r').".tex"
	let tmptexfilename = expand('%:t:r').".tex.tmp"
	if(filereadable(texfilename))
		" Delete references section and code blocks from word count
		execute " !sed '/begin{Shaded}/, /end{Shaded}/d' " . texfilename . " > " . tmptexfilename
		execute " !sed '/\\\\begin{cslreferences}/, /\\\\end{cslreferences}/d' -i " . tmptexfilename
		execute " !sed '/\\\\.*section/d' -i " . tmptexfilename
		execute " !sed '/^\\\\begin\{verbatim\}/,/^\\\\end\{verbatim\}/d' ". tmptexfilename . " \| detex " " \| wc -w"
		execute " !rm " . tmptexfilename
	endif
endfunction

" Orgmode Mappings
nnoremap <leader>ieq a$$<ESC>i
nnoremap <leader>=> a\Rightarrow<ESC>
nnoremap <leader><= a\Leftarrow<ESC>
nnoremap <leader>== a\Leftrightarrow<ESC>
nnoremap <leader>src i```<CR><++><CR>```<ESC>2kA
