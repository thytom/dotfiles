" Vim/nVim .vimrc
" Author: Archie Hilton
" Email	:	archie.hilton1@gmail.com

" Leader Keys
let mapleader=','
let maplocalleader='\'

syntax on
filetype plugin indent on

" vim-plug
source /home/archie/.config/nvim/plugins.vim
let g:pandoc#syntax#codeblocks#embeds#langs = ['c', 'haskell', 'python', 'cpp', 'bash', 'css', 'javascript', 'latex=tex', 'php', 'makefile=make', 'lisp', 'perl', 'yaml', 'sh', 'html']
let g:pandoc#formatting#textwidth = 80
let g:pandoc#formatting#mode = "h"

" Lightline Configuration
source /home/archie/.config/nvim/wordcount.vim
let g:lightline = {
      \ 'active': {
      \   'right': [ [ 'lineinfo' ], [ 'percent', 'wordcount' ], [ 'fileformat', 'fileencoding', 'filetype' ] ]
      \ },
      \ 'component_function': {
      \   'wordcount': 'WordCount',
      \ },
      \ }

" Some basics:
colorscheme dracula
set background=dark
hi Normal guibg=none ctermbg=none
set nocompatible
set encoding=utf-8
set mouse=a
set number
set splitbelow splitright
set nohlsearch
set tabstop=4
set noexpandtab
set shiftwidth=0
set ignorecase
set smartcase
set wrap
set linebreak
set visualbell
set numberwidth=6
set foldlevelstart=99
set vb t_vb=

" Language Imports
source /home/archie/.config/nvim/lang/haskell.vim
source /home/archie/.config/nvim/lang/org_rmd_tex.vim

" NERDTree
nnoremap <leader>r :NERDTreeToggle<CR>

" Disable autocomment on newline
au FileType * setlocal formatoptions-=r formatoptions-=o

au FileType c,cpp nnoremap <buffer> <leader>g :call CXXIDE()<CR>

au FileType tex,md,rmd,c,cpp,pandoc set textwidth=80

" Highlight 81st column
au FileType c,cpp,sh,python,lisp call HLCC ()
function! HLCC ()
	highlight ColorColumn ctermbg=yellow
	call matchadd('ColorColumn', '\%81v', 100)
endfunction

" Don't wrap HTML
au FileType html set nowrap

" au BufRead,BufNewFile *.md set tw=79

" au BufWritePre * %s/\s\+$//e
au BufWritePost ~/.Xresources,~/.Xdefaults !xrdb %

"Normal Mappings
nnoremap ; :
nnoremap : ;
nnoremap <SPACE><SPACE> /<++><CR>c4l
nnoremap <leader>s :set spell!<CR>
nnoremap <tab> za
nnoremap gy :Goyo<CR>

"" Make tabs, trailing whitespace and non-breaking spaces visible
exec "set listchars=tab:\uBB\uBB,trail:\uB7,nbsp:~"
nnoremap & :set list!<CR>

"" Split
nnoremap \| :vs<CR>
nnoremap _ :sp<CR>
nnoremap <C-h> <C-w>h
nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-l> <C-w>l

" Visual Mappings
xnoremap > >gv
xnoremap < <gv

xnoremap <C-c> "+y
nnoremap <C-p> "+P

" Terminal Mappings
tnoremap <Esc> <C-\><C-n>

" Personal IDE Code
let g:ide_active=0

function! HsIDE()
	if g:ide_active==1
		silent! exec "GhcidKill"
		exec "%bd!|e#"
		let g:ide_active=0
	else
		exec "NERDTree | wincmd l | 60vs  term://ghci % | set syntax=haskell | wincmd h | Ghcid \"" . expand('%:p') . "\""
		let g:ide_active=1
	endif
endfunction

function! CXXIDE()
	if g:ide_active==1
		exec "%bd!|e#"
		let g:ide_active=0
	else
		exec "botright 10sp term://zsh | wincmd k | NERDTree | wincmd l"
		let g:ide_active=1
	endif
endfunction
