# XMONAD & XMOBAR
alias xhs='vim ~/.xmonad/xmonad.hs'
alias xmb='vim ~/.config/xmobar/xmobarrc'

alias bt='bluetoothctl'

alias please='sudo'
alias fuck='sudo $(fc -ln -1)'
alias vifm='/home/archie/.config/vifm/scripts/vifmrun'
alias r='/home/archie/.config/vifm/scripts/vifmrun'

alias weather='curl v2.wttr.in'

alias mkd='/usr/bin/mkdir'

# alias i3c='nvim /home/archie/.config/i3/config'

alias nb='newsboat'
alias nburls='nvim /home/archie/.config/newsboat/urls'

alias nm='neomutt'

alias n='ncmpcpp'

alias q='exit'

# alias r='ranger'

alias sp='sudo pacman -S'
alias u='yay'

alias t='tmux'
alias ta='tmux attach -t'
alias tl='tmux list-sessions'

alias trc='transmission-remote-cli'

alias v='nvim'
alias vim='nvim'
alias vimrc='nvim /home/archie/.vimrc'
alias vp='nvim /home/archie/.config/nvim/plugins.vim'

alias yt='youtube-dl'

alias e='emacsclient --create-frame'

alias todo='emacs ~/Notes/Todo/todo.org'

shorten() {
	curl -s "http://tinyurl.com/api-create.php?url=${1}"
	echo
}
