set background=dark
hi clear
if exists("syntax_on")
  syntax reset
endif
let g:colors_name = "archie"
hi Normal		guifg=cyan	guibg=black
hi NonText		guifg=yellow guibg=#303030
hi Comment		term=bold		ctermfg=Grey		guifg=#80a0ff
hi constant		guifg=cyan	gui=bold
hi identifier		guifg=cyan	gui=NONE
hi statement		guifg=lightblue	gui=NONE
hi preproc		guifg=Pink2
hi type			guifg=seagreen	gui=bold
hi special		guifg=yellow
hi ErrorMsg		guifg=Black	guibg=Red
hi WarningMsg		guifg=Black	guibg=Green
hi Error		guibg=Red
hi Todo			guifg=Black	guibg=orange
hi Cursor		guibg=#60a060 guifg=#00ff00
hi Search		guibg=darkgray guifg=black gui=bold
hi IncSearch		gui=NONE guibg=steelblue
hi LineNr		guifg=darkgrey
hi title		guifg=darkgrey
hi ShowMarksHL 		ctermfg=cyan ctermbg=lightblue cterm=bold guifg=yellow guibg=black  gui=bold
hi StatusLineNC		gui=NONE guifg=lightblue guibg=darkblue
hi StatusLine		gui=bold	guifg=cyan	guibg=blue
hi label		guifg=gold2
hi operator		guifg=orange
hi clear Visual
hi Visual		term=reverse cterm=reverse gui=reverse
hi DiffChange   	guibg=darkgreen
hi DiffText		guibg=olivedrab
hi DiffAdd		guibg=slateblue
hi DiffDelete   	guibg=coral
hi Folded		guibg=NONE ctermbg=NONE ctermfg=Grey
hi FoldColumn		guibg=gray30 guifg=white ctermbg=none
hi cIf0			guifg=gray
hi diffOnly		guifg=red gui=bold
hi Title		ctermfg=cyan cterm=bold gui=bold guifg=magenta
hi mkdHeading	ctermfg=darkgrey
hi Conceal ctermbg=none

" vim-orgmode formatting
hi org_title 			ctermfg=white cterm=bold,underline gui=bold guifg=white
hi org_heading1			ctermfg=cyan cterm=none
hi org_heading2			ctermfg=lightgreen cterm=none
hi org_heading3			ctermfg=yellow cterm=italic

hi org_key_identifier	ctermfg=darkgrey
hi org_shade_stars		ctermfg=darkgrey
hi texMathSymbol				ctermbg=none
hi statement			ctermbg=none
