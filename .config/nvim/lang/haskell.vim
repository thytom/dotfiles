" Haskell stuff
let g:haskell_indent_disable=0
let g:hindent_on_save = 0
let g:hindent_command = "~/.local/bin/hindent"
let g:hindent_line_length = 80
au BufEnter *.hs compiler ghc
au FileType haskell
				\ syn match haskellLambda '\\' conceal cchar=λ |
				\ set conceallevel=2 concealcursor=nv |
				\ setlocal tabstop=2 shiftwidth=2 expandtab |
				\ hi clear Conceal |
				\ nnoremap <buffer> <leader>f :Hindent<CR> |
				\ nnoremap <buffer> <leader>g :call HsIDE()<CR>
